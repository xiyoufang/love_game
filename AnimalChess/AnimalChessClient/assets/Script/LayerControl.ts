export default class LayerControl {

    private static _inst: LayerControl = null;
    /**
     * 上一个Layer
     */
    private _preLayer: cc.Component = null;

    /**
     * 获取单例
     */
    static inst(): LayerControl {
        if (this._inst == null) {
            this._inst = new LayerControl();
        }
        return this._inst;
    }

    /**
     * 切换Layer
     * @param layer
     */
    selectLayer(layer: typeof cc.Component) {
        let _currentLayer = fgui.GRoot.inst.node.addComponent(layer);
        if (this._preLayer != null) {   //如果上一个Layer不为null
            this._preLayer.destroy();
        }
        this._preLayer = _currentLayer;
    }

}
