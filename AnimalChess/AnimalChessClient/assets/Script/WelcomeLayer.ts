import ccclass = cc._decorator.ccclass;
import LayerControl from "./LayerControl";
import GameLayer from "./GameLayer";

@ccclass
export default class WelcomeLayer extends cc.Component {

    /**
     * 界面组件
     */
    private _view: fgui.GComponent;

    protected onLoad(): void {
        console.log("WelcomeLayer onLoad");
        fgui.UIPackage.loadPackage("UI/Animal", () => { //加载资源包
            this._view = fgui.UIPackage.createObject("Animal", "WelcomeLayer").asCom;
            this._view.makeFullScreen();
            fgui.GRoot.inst.addChild(this._view);
            this._view.getChild("StartGameBtn").asButton.onClick(() => {             //开始游戏按钮绑定事件
                LayerControl.inst().selectLayer(GameLayer);
            }, this);
        });
    }

    /**
     * 释放资源
     */
    protected onDestroy(): void {
        console.log("WelcomeLayer onDestroy");
        this._view.dispose();
    }
}
