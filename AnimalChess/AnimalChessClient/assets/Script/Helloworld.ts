import LayerControl from "./LayerControl";
import WelcomeLayer from "./WelcomeLayer";
import FireKit from "./fire/FireKit";
import Config from "./Config";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {

    onLoad() {
        fgui.addLoadHandler();
        fgui.GRoot.create();
        this.initFire();
        LayerControl.inst().selectLayer(WelcomeLayer);  // 选择欢迎界面
    }

    /**
     * 初始化事件驱动模块
     */
    initFire() {
        FireKit.init(Config.AI_FIRE);
        FireKit.init(Config.HUMAN_FIRE);
    }
}
